<?php

function htmlEnv($out)
{
    return '
<HTML>
<HEAD>
  <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
</HEAD><BODY>
'.$out.'
</BODY></HTML>
';
}

function fixEncoding($out) {
    return str_replace(
        array("„","“","–"), array("&#8222","&#8221","&ndash;"), $out
    );
}

function createLink($url,$text) {
    return '<a href='.$url.'>'.$text.'</a>';
}

function setNameSpace() {
    EasyRdf_Namespace::set('foaf', 'http://xmlns.com/foaf/0.1/');
    EasyRdf_Namespace::set('owl', 'http://www.w3.org/2002/07/owl#');
    EasyRdf_Namespace::set('org', 'http://www.w3.org/ns/org#');
    EasyRdf_Namespace::set('qb', 'http://purl.org/linked-data/cube#');
    EasyRdf_Namespace::set('hmd', 'http://hmt-leipzig.de/Data/Model#');
    EasyRdf_Namespace::set('cc', 'http://creativecommons.org/ns#');
    EasyRdf_Namespace::set('dct', 'http://purl.org/dc/terms/');
    EasyRdf_Namespace::set('gnd', 'https://d-nb.info/gnd/');
}


function navigation() {
    $offset=isset($_GET["offset"]) ? $_GET["offset"] : 0;
    $entries=isset($_GET["entries"]) ? $_GET["entries"] : 20;
    $action=isset($_GET["action"]) ? $_GET["action"] : "all";
    $src=$_SERVER['SCRIPT_NAME'];
    $backwardoffset=$offset-$entries;
    if ($backwardoffset<0) { $backwardoffset=0; }
    $forwardoffset=$offset+$entries;

    $out='
    <!-- <form method="GET">
    <b>Anzahl der Elemente</b>
    <select name="entries" size="1" onchange="this.form.submit()">
    <option>20</option>
    <option>50</option>
    <option>100</option>
    </select></form>-->
    <div align="left">
    <a href="'.$src.'?offset='.$backwardoffset.'&entries='.$entries.'&action='.$action.'">Zurück</a>
    </div>
    <div align="right">
    <a href="'.$src.'?offset='.$forwardoffset.'&entries='.$entries.'&action='.$action.'">Vorwärts</a>
    </div>';

    return $out;
}
