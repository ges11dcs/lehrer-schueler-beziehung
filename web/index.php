<?php
/**
 * User: Hans-Gert Gräbe
 * Date: 2019-10-10
 */

include_once("layout.php");

$content='
<div class="container">
<div class="row">
<div  class="col-lg-1 col-sm-1"></div><div  class="col-lg-10 col-sm-10">

<h2 align="center"> Zum Hintergrund und zur Methodik </h2>

<p>Während die Namen der 6200 Schüler auf Grund des gut lesbaren Kopfes der
7715 Zeugnisse (zu einigen Schülern lagen mehrere Zeugnisse vor, teilweise
Vor- und Reinschriften) leicht zu ermitteln waren (und zu Projektbeginn bereits
in einer CSV-Datei vorlagen), ist dies bei den 55 Lehrern schwieriger, da die
Zuordnung zu Schülern nur an Hand der in den meisten Fällen kaum zu
entziffernden Unterschrift erfolgen kann.</p>

<p>Dazu wurden die relevanten Bereiche auf den Zeugnissen in 30610
Bilddatei-Schnipsel zerlegt, die jeweils eine Unterschrift enthielten, von der
klar war, zu welchem Fach und Schüler sie gehört. Auf diesem Rohmaterial wurde
die Mustererkennung mit KI-Technologien weiter ausgeführt.</p>

<p>Die Bilder wurden dabei mit bewährten Bildverarbeitungsverfahren auf "das
Wesentliche" reduziert. Diese Bilddateien (der Größe 75x26 Pixel) bildeten das
Ausgangsmaterial für den mit einem konvolutionalen neuronalen Netz
durchgeführten Lernprozess. Details zum Aufbau des Netzes sowie zu den
verwendeten Software-Werkzeugen sind in der <a href="masterarbeit.pdf"
>Masterarbeit</a> ausgeführt.</p>

<p>Hier werden die Ergebnisse des Projekts präsentiert. </p>

<div class="col-lg-1 col-sm-1"> </div> </div> </div>

';
echo showPage($content);

?>
