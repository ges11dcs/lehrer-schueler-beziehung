<?php
/**
 * User: Hans-Gert Gräbe
 * last update: 2019-07-09
 */

function pageHeader() {
  return '
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="description" content="HDM Demonstration Site"/>
    <meta name="author" content="Lehrerunterschriften"/>

    <title>Lehrerunterschriften</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>

  </head>
<!-- end header -->
  <body>

';
}

function pageNavbar() {
  return '

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default" role="navigation">
      <div class="container">
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Hintergrund</a></li>
            <li><a href="lehrer.php">Lehrer</a></li>
            <li><a href="zeugnisse.php">überprüfte Zeugnisse</a></li>
            <li><a href="schueler.php">Schüler</a></li>
          </ul>
        </div><!-- navbar end -->
      </div><!-- container end -->
    </nav>';
}

function generalContent() {
  return '
<div class="container">
<div class="row">
<div  class="col-lg-1 col-sm-1"></div><div  class="col-lg-10 col-sm-10">

<h1 align="center">Historische Lehrer-Schüler-Beziehungen an der Hochschule
für Musik und Theater (HMT) Leipzig</h1>

<p>In der Bibliothek der HMT sind viele historische Dokumente vorhanden, u.a.
eine größere Anzahl von Zeugnissen, auf denen den Schülern ihre Leistungen
attestiert wurden.  Ein solches Zeugnis enthält in der Regel schriftliche
Kurzbewertungen des Schülers in vier Fächern. <strong>Ziel des
Projektes</strong> war es, aus den in eingescanter Form als Bilddateien
vorliegenden Zeugnissen mit Mitteln der allseits beschworenen "künstlichen
Intelligenz" aus den Lehrerunterschriften unter diese Bewertungen eine
Lehrer-Schüler-Zuordnung zu extrahieren.</p>

<div class="col-lg-1 col-sm-1"> </div> </div> </div>
';
}

function pageFooter() {
  return '

      <div class="container">
    <div class="footer">
        <p class="text-muted">&copy; Tobias Wieprich, Uni Leipzig 2019 </p>
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap JavaScript plugins) -->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

  </body>
</html>';
}

function showPage($content) {
  return pageHeader().generalContent().pageNavbar().($content).pageFooter();
}
