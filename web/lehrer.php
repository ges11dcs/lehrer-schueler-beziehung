<?php
/**
 * User: Tobias Wieprich
 * last update: 2019-10-10
 */

require_once 'lib/EasyRdf.php';
require_once 'helper.php';
include_once("layout.php");

function dieLehrer($in) {
    setNameSpace();
    $graph = new EasyRdf_Graph('http://hmt-leipzig.de/Data/Lehrer/');
    $graph->parseFile($in);
    $a=array();

    $res = $graph->allOfType('foaf:Person');
    foreach ($res as $lehrer) {
        $name = "";
        $gnd="";
        $id=$lehrer->getUri();
        foreach ($lehrer->all("hmd:Titel") as $e) {
            $titel='<span itemprop="name" class="foaf:title">'
                .$e->getValue().'</span>';
        }
        foreach ($lehrer->all("foaf:firstname") as $e) {
            $vorname='<span itemprop="name" class="foaf:firstname">'
                .$e->getValue().'</span>';
        }
        foreach ($lehrer->all("foaf:lastname") as $e) {
            $name='<span itemprop="name" class="foaf:lastname">'
                .$e->getValue().'</span>';
            $name = $e->getValue();
        }
        foreach ($lehrer->all("gnd:Number") as $e) {
            $gnd=createLink($e,"GND: $e");
        }
        $a[$name.$id]=
            "<tr><td>$name;  $vorname</td> <td> $titel </td> <td>$gnd</td> </tr>";
    }
    ksort($a);


    $out='<h3 align="center">Lehrer an der HMT</h3>
<div class="people"><p>
<table align="center" border="1">
<thead> <tr> <th> Name </th> <th> Titel </th> <th> GND-Nummer </th> </tr>
'.join("\n", $a).'
</table></p>
</div> <!-- end class people -->';
    return '
<div class="container">
'.$out.'
</div>
';
}

echo showPage(dieLehrer("rdf/Lehrer.rdf"));
#echo dieLehrer("rdf/Lehrer.rdf")
?>
