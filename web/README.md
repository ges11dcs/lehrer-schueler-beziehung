# Template für eine Webpräsenz der Ergebnisse der Arbeit.

## Verzeichnis rdf

Dorthin werden mit dem Makefile die relevanten RDF-Dateien im rdfxml-abbrev
Format kopiert, da EasyRDF das Turtle-Format im Gegensatz zu RDF/XML sehr
langsam parst.

Die Dateien wurden mit ins Repo aufgenommen, obwohl es sich um Sekundärdateien
handelt, um die Anwendung in sich abgeschlossen zu halten.

## Verzeichnisse Bilddateien-Klein und Biddateien-Gross

In diese Verzeichnisse sollen die jeweils 12000 Bilddateien kopiert werden wie
am 11.10.2019 besprochen, um diese in der Datenübersicht zu referenzieren.
Die Dateinamen sind in der RDF-Übersicht der Einheiten (siehe README im
Verzeichnis Daten/Cube) zu erfassen, wo drin steht, mit welchem Zeugnis und
welchem Fach auf diesem Zeugnis sie verknüpft sind.

Das ist nun aus der früheren Datei *unzip_images.sh* auch in das einheitliche
Makefile integriert.

## Präsentationen

Lehrer: Übersicht über die Lehrer (erste rohe Implementierung als Proof of
Concept) samt vorhandenen Detailangaben. Irgendwie sinnvoll alphabetisch
sortieren.

Schüler: Ähnliche Übersicht über die Schüler.

Datenübersicht (ggf. mehrere, weitere Links in layout.php anlegen): Diese
bezieht sich auf das Ergebnis *eines* kompletten Durchlaufs. Zu jeder der 6000
Bilddateien wird in angemessener Form dargestellt, welche Lehrer diese
Unterschrift möglicherweise produzert haben.

Die Tabelle enthält wenigstens folgende Spalten:

* Kleines Image der Unterschrift
* Bilddaten-ID
* Präsentation des Ergebnisses

* Wirklicher oder geratener Lehrer mit Konfidenzangabe 0..1 (wenn manuell
  geprüft, dann Konfidenz=1)

und sollte nach einem sinnvollen Kriterium sortierbar sein.

Die Tabelle soll Ausgangspunkt für ein interaktives Formular sein, mit dem
Frau Jaksch die Datenqualität manuell bis zu 100% weiter verbessern kann.

Die letzten beiden Punkte sind zunächst prototypisch bereitzustellen, um dann
im Kundengespräch die genauen Forderungen aufzunehmen und umzusetzen.

