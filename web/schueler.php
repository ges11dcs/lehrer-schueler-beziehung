<?php
/**
 * User: Tobias Wieprich
 * last update: 2019-10-21, HGG
 */

require_once 'lib/EasyRdf.php';
require_once 'helper.php';
include_once("layout.php");

function show_schueler($in){
  setNameSpace();
  $graph = new EasyRdf_Graph('http://hmt-leipzig.de/Data/Schueler/');
  $graph->parseFile($in);
  $a=array();
  $res = $graph->allOfType('foaf:Person');
  foreach ($res as $schueler) {
      $name = "";
      $id=$schueler->getUri();
      foreach ($schueler->all("foaf:firstname") as $e) {
          $vorname='<span itemprop="name" class="foaf:firstname">'
              .$e->getValue().'</span>';
      }
      foreach ($schueler->all("foaf:lastname") as $e) {
          $name='<span itemprop="name" class="foaf:lastname">'
              .$e->getValue().'</span>';
      }
      $zeugnis='<span itemprop="name" class="hmd:Inskription">'
          .str_replace("http://hmt-leipzig.de/Data/Person/Schueler/","" , $id).'</span>';
      $a[$id]= "<td> $vorname $name</td><td> $zeugnis </td>";
  }
  ksort($a);
  return $a;
}

function schueler_nav($in) {
    $offset=isset($_GET["offset"]) ? $_GET["offset"] : 1;
    $entries=isset($_GET["entries"]) ? $_GET["entries"] : 20;
    $out='<h3 align="center">Ehemalige Schüler an der HMT</h3>
         '.navigation().'
         <table align="center" width="70%" border="1">
         <thead> <tr> <th> ID </th> <th> Name </th> <th> Matrikelnummer</tr>';
    $a=show_schueler($in);
    for($i=0;$i<$entries;$i++) {
        $id='http://hmt-leipzig.de/Data/Person/Schueler/'.($offset+$i);
        $out.="<tr><td>$id</td>".$a[$id]."</tr>";
    }
    $out.='</table> <!-- end table entries -->';
    return '<div class="container">'.$out.'</div>';
}

echo showPage(schueler_nav("rdf/Studenten.rdf"));
?>
