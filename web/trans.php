<?php
/**
 * User: Hans-Gert Gräbe
 * last update: 2019-10-10

Extract some fields from an RDF File

 */

require_once 'lib/EasyRdf.php';
require_once 'helper.php';

function getFields($in){
  setNameSpace();
  $graph = new EasyRdf_Graph('http://example.org/MyExample/');
  $graph->parseFile($in);
  $a=array();
  $res = $graph->allOfType('qb:Observation');
  foreach ($res as $v) {
      $id=$v->get("hmd:id");
      $region=$v->get("hmd:region");
      $coordinates=$v->get("hmd:coordinates");
      $schueler=$v->get("hmd:Student");
      $a[]=
          "$id | $region | $coordinates | $schueler";
  }
  // ksort($a);
  return $a;
}

echo join("\n",getFields("rdf/Beziehung_200.rdf"));
?>
