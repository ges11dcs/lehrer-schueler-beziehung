<?php
/**
 * User: Tobias Wieprich
 * last update: 2019-10-27
 */

require_once 'lib/EasyRdf.php';
require_once 'helper.php';
include_once("layout.php");

function get_lehrer_name($lehrer_id){
  $lehrer = $array = array(
    42 => "Klengel",
    54 => "Rochterkopf",
    55 => "Barfuß",
    53 => "Gabe",
    52 => "Graubau",
    51 => "Böhme",
    50 => "Becker",
     7 => "Richter",
    31 => "Plaichi",
    47 => "Albert",
    36 => "Mangel",
    39 => "Hauptmann",
    28 => "David",
    57 => "Mochelly",
    44 => "Mendelssohn",
  );
  return $lehrer[$lehrer_id];
}

function korrekte_Zeugnisse($graph){
  $lehrer_search=isset($_GET["lehrer"]) ? $_GET["lehrer"] : null;
  $a=array();
  $res = $graph->allOfType('hmd:Schnipsel');
  $number = 0;
  foreach ($res as $zeugnis) {
    $lehrer = $zeugnis->get("hmd:Klasse");
    $lehrer = str_replace("http://hmt-leipzig.de/Data/Person/Lehrer/", "", $lehrer);
    if ($lehrer_search !== null && $lehrer_search !== get_lehrer_name($lehrer) )
        continue;
    $id=$zeugnis->getUri();
    $schnipsel = explode("/", str_replace("http://hmt-leipzig.de/Schnipsel/", "",$id ));

    $schueler = $schnipsel[0];
    if ($schnipsel[2] === "0") {
      $schnipsel = $schnipsel[0]."_".$schnipsel[4];
    } else {
        $schnipsel = $schnipsel[0]."_".$schnipsel[2]."_".$schnipsel[4];
    }
    $image = '<img src="Bilder/manuel_validierte/'.$schnipsel.'.jpg"  width="70" height="70">';


    $a[$number] = "<tr><td>".$image."</td><td>".$schnipsel."</td><td>".get_lehrer_name($lehrer)."</td><td>".$schueler."</td></tr>\n";
    $number += 1;
  }
  return  $a;
}
function dieZeugnisseNav() {
  setNameSpace();
  $graph = new EasyRdf_Graph('http://hmt-leipzig.de/Data/Zeugnisse/');
  $graph->parseFile("rdf/Schnipsel.rdf");
  $graph->parseFile("rdf/Studenten.rdf");
    $offset=isset($_GET["offset"]) ? $_GET["offset"] : 0;
    $entries=isset($_GET["entries"]) ? $_GET["entries"] : 20;
    $out='<h3 align="center">Lehrer in den Zeugnissen</h3>
         <form class="Lehrersuche" action="./zeugnisse.php"
         style="margin:auto;max-width:300px">
         <input type="text" placeholder="Search.." name="lehrer">
         <button type="submit">Suche<i class="fa fa-search"></i></button>
         </form>
          <div class="people"><p>'.navigation().'
          <table align="center" border="1">
          <thead> <tr> <th> Bild </th> <th> Schnipsel </th> <th>  Lehrer
          </th><th> Student </th> </tr>';
    $a=korrekte_Zeugnisse($graph);
    ksort($a);
    if (count($a) < $entries){
      $entries = count($a);
    }
    for($i=0;$i<$entries;$i++) {
        $out.=$a[$i+$offset];
    }
$out.='
</table> <!-- end table entries -->';
  return '
<div class="container">
'.$out.'
</div>
';
  return $out;
}

echo showPage(dieZeugnisseNav());
