## Abhänigkeiten
Es wird Docker Engine 1.10 oder höher benötigt.

Die Version kann mit ```docker -version``` überprüft werden.


## Installation
Mit ```docker-compose up``` können die Container erstellt werden.

## Daten
Die Zeugnisse müssen im Ordner Daten/Ressources/Zeugnisse liegen. Die Daten können mit ```sh download_ressources.sh``` runtergeladen werden. Dies kann je nach Internetverbindung dauern.


## Ausführen
Das Programm kann mit ```docker-compose run signature_detector python main.py``` ausgeführt werden. Dabei wird aktuell alles auf die Konsole ausgegeben.


## Fehlermeldung

```
ERROR: Couldn't connect to Docker daemon at http+docker://localunixsocket - is it running?
```

Wenn dieser Fehler auftaucht, müssen die Befehle mit Sudo Rechten ausgeführt werden.
