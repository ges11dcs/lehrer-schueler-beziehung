### Beobachtung über die Dateien:
* alle Bilddateien sind über 1 MB groß, egal ob es Fragmente oder Zeugnisse sind.
  Dummies sind im KB bereich. Erkennung zwischen Zeugnis und Dummies kann
  durch Überprüfung der Dateien größe gesehen. Beispiel:
* Pixelanzahl ist nicht konstant, sonderen schwangt leicht. Dummies haben
  wesentlich weniger Pixel als normale Bilddateien
* Rahmen der Zeugnisse wechseln die Rahmen. Es gibt insgesammt 3 unterschiedliche Rahmen.
* Dummydatei hat folgenden Inhalt: Matrikel, Name des Stunden, "Zeugnis nicht vorhanden"
* Dateien mit Namesmuster: Kopie_von_XXX.jpg sind Artefakte und könnten ignoriert werden
* Einige Dateien beinhalten Fragmente und beschriebene Rückseiten.

### Beobachtung über Fächer
* Einige Fächer wie Orgelspiel tauchen auf alle Zeugnissen auf
* Ab einen bestimmten Zeitpunkt werden Fächer wie Vorlesung Standart
* Stimmt nicht mit Fächernamen in der Lehrerliste über ein, aber wahrscheinlich
 andere Namen beipiel Vorlesung italische Sprache und Übung italnische Sprache oder
 Teilfächer
