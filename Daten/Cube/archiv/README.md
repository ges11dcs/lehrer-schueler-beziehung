11.10.2019: In diesem Verzeichnis sollten (nur) die Ergebnisse der
verschiedenen Experimente als einzelne RDF-Cubes liegen.  Gemeinsame
DataSet-Definitionen sollten in eine spezielle ttl ausgelagert und in den
relevanten Cubes per owl:import importiert werden.

Weitere relationale Informationen über

Studenten-IDs
Zeugnis-IDs
Einheiten-IDs ( = Bewertungseintrag auf einem Zeugnis )
Lehrer-IDs
Fach-IDs

sollten im Verzeichnis RDF abgelegt werden.

Zu den geschätzt 12.000 Einheiten-IDs sind folgende Informationen relevant
- Zeugnis-ID
- Fach-ID
- gibt es dort eine Unterschrift oder nicht (das kann ggf. aus der Existenz
  entsprechender Einträge inferiert werden)
- Dateiname Unterschriftsbild-Klein (75x25)
- Dateiname Unterschriftsbild-Groß (128x75 - Graustufenbild)
- manuell herausgefundener Lehrer (if any)