# Das Datenmodell

## Allgemeines

Über die vorliegenden Zeugnisse sind Lehrer, Schüler und Unterrichtsfächer
miteinander verknüpft.  Metadaten zu den entsprechenden Instanzen sind im
Verzeichnis RDF in den Dateien

* Faecher.ttl
* Lehrerregister.ttl
* Studentenregister.ttl
* Zeugnisregister.ttl

zusammengetragen. In einem ersten Arbeitsschritt wurden die Zeugnisse in
_Schnipsel_ zerlegt, so dass auf jedem Schnipsel nur ein Fach enthalten ist,
über das eine eindeutige Zuordnung von Lehrer (entsprechend seiner
Unterschrift) und Schüler (nach den Angaben auf dem Zeugnis, diese waren
bereits digital erfasst).

Die Datei `metadata.ttl` enthält die Zuordnung zwischen Schnipseln,
Bilddateien und Schülern, die Datei `Schnipselregister.ttl` eine genauere
Beschreibung der Schnipsel, die als Bilddateien in einem eigenen Verzeichnis
abgelegt sind. Die Schnipsel-Bilddateien sind dabei im Rahmen dieses Projekts
auf die Unterschrift des jeweiligen Lehrers reduziert.

## Namensräume

Neben den RDF-Basisontologien sowie verbreiteten Ontologien wie

* cc - Beschreibung von Offenen Lizenzen
* dct - Beschreibung von Dublin Core 
* foaf - Beschreibung von Personen
* geo - geografische Namensgebung
* gnd - Beschreibung der GND - General Norm Data der DNB
* org - Beschreibung von Organisationen

wird ein eigener Namensraumpräfix $hmt = http://hmt-leipzig.de/Data für die
Definition von Instanzen und Modellen im Rahmen dieses Projekts verwendet.

* $hmt/Model# - Begriffsontologie des Projekts

## Fächer

Enthält 23 Instanzen.

* Namensschema $hmt/Fach/<nr>
* RDF-Klasse hmd:Fach
* Prädikate
  - hmd:Name Literal - Bezeichnung, Beispiel: "Theorie_der_Musik_und_Compostion"
  - hmd:AlleZeugnisVorhanden Boolean - Bedetung zu klären
  - dct:modified Datum - Datum der letzten Modifikation
  
## Lehrer

Enthält 55 Instanzen.

* Namensschema $hmt/Person/Lehrer/<nr>
* RDF-Klasse foaf:Person 
* Prädikate
  - foaf:lastname Literal - Nachname
  - foaf:title Literal - Titel, Beispiel: "Freiherr"
  - hmd:Number URI - GND-Eintrag
  - hmd:Rang Literal - Rang, Beispiel: "Staatsminister a.D."
  - hmd:Status Literal - Status am HMT, Beispiel "Directorium"
  - dct:modified Datum - Datum der letzten Modifikation

## Metadaten

Enthält 30610 Instanzen.

* Namensschema $hmt/Model/metadata/id/<nr>
* RDF-Klasse - keine
* Prädikate 
  - hmd:File Literal - Name der Bilddatei (wovon?), Beispiel "00001531_1.jpg" 
  - hmd:Schnipsel Integer - zu erklären, Beispiel "2" 
  - hmd:Schueler URI - URI des Schülers

## Schnipsel

Enthält 1874 Instanzen.

* Namensschema http://hmt-leipzig.de/Schnipsel/00000002/Number/0/ID/11
  - HGG: schlechtes Namensschema, da der allgemeine Präfix des Projekts nicht 
    verwendet wird.
* RDF-Klasse hmd:Schnipsel
* Prädikate
  - hmd:Klasse URI - URI des Lehrers, wenn dieser manuell verifiziert wurde
  - hmd:Zeugnis URI - URI des Zeugnisses 
  - hmd:coordinates Literal - Bereich auf dem Original, Beispiel "[8, 1408, 69, 1408]" 

## Studenten
Enthält 6200 Instanzen.

* Namensschema $hmt/Person/Schueler/<nr>
* RDF-Klasse foaf:Person 
* Prädikate
  - hmd:Inskription Literal - zu klären, Beispiel "7" 
  - hmd:JahrInskription Year - Jahr der Einschreibung, Beispiel "1843" 
  - geo:CountryModern Literal - Herkunft, moderne Bezeichnung, Beispiel "Deutschland" 
  - geo:CountryOld Literal - Herkunft, alte Bezeichnung, Beispiel "Sachsen" 
  - foaf:firstname Literal - Vorname 
  - foaf:lastname Literal - Nachname

## Zeugnisse

Enthält 7715 Instanzen.

* Namensschema $hmt/Model/zeugnis/id/00000001/number/1 - zu klären
* RDF-Klasse - keine
* Prädikate
  - hmd:Height Integer - Höhe des Bildes in px
  - hmd:Schueler <http://hmt-leipzig.de/Data/Person/Schueler/00000001> ;
  - hmd:Width Integer - Breite des Bildes in px
  - hmd:isDummy Boolean - ist das eine Dummydatei?
  - hmd:sizeMB float - Dateigröße

