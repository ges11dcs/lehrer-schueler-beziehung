import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
img = cv.imread('Auenstein-fachwerk2.jpg',0)

ret2,th2 = cv.threshold(img,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
images = [img, 0, th2,]
plt.subplot(2,2,1)
plt.title("orginal Bild")
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
plt.subplot(2,2,2)
plt.title("binäres Bild")
plt.imshow(th2, cmap = 'gray', interpolation = 'bicubic')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
#plt.show()

plt.savefig('haus_binary.png')

#plt.save("")
