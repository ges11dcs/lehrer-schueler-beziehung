import cv2 as cv
import numpy as np

img = cv.imread('Auenstein-fachwerk2.jpg')
imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)

ret2,th2 = cv.threshold(imgray,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)

im2, contours, hierarchy = cv.findContours(th2,cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
cv.drawContours(img, contours, -1, (0,255,0), 3)
cv.imwrite("test.png", img)
