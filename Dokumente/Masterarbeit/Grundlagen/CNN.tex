\section{Neuronales Netz}
Im diesen Abschnitt werden die wichtigsten Begriffe für neuronale Netze 
erläutert.


\subsection{Funktionsweise eines neuronalen Netzes}
\label{subsec:NN}

Das erste künstliche Neuron wurde von McCulloch und  Pitts  im Jahre 1943 veröffentlicht
und konnte bereits logische und arithmetische Funktionen berechnen \cite{cowan1990discussion}.
Allerdings konnte das Modell, welches nach ihren Erfindern McCulloch-Pitts-Neuron
benannt wurde, nicht trainiert werden.
Die Gewichte des Neurons waren nach der Erstellung des Modells fest.

1949 notierte Hebb in seinen Werk \glqq The organization of behavior\grqq{} die
erste Lernregel zwischen Neuronen \cite{hebb}.
Die Regel besagt, dass Neuronen, die gleichzeitig feuern, werden bevorzugt
gleichzeitig reagieren.
Diese Lernregel wurde mathematisch abstrahiert %nachschlagen
und ist in (\ref{Formel:Hebb}) zu sehen.
$w_{ij}$ ist  das Gewicht zwischen den Neuron i und j,
$\alpha _i$/$\alpha _j$ die Aktivierung des Neurons i bzw j und
$\eta$ ist die Lernrate, eine Konstante, welche am Anfang festgelegt wird.
Diese bildete die erste Grundlage für das Training von neuronalen Netzen.

\begin{equation}
\begin{gathered}
\delta w_{ij} = \eta * \alpha _i * \alpha _j
\end{gathered}
\label{Formel:Hebb}
\end{equation}


\begin{figure}[ht]
 \centering
  \includegraphics[width=0.5\textwidth]{Grundlagen/images/neuronales_Netz.png}
 \caption[künstliches Neuron]{Diese Abbildung zeigt ein einfaches künstliches
 Neuron. $w_n$ bildet das n-te Gewicht, $x_n$ der n-te Eingang, $\theta$ die Summation und 
 die Aktivierungsfunktion und $y$ die Ausgabe.
 Quelle: \cite{netze}}
 \label{fig:neuron}
 \end{figure}

Das grundlegende Modell, welches heute noch benutzt wird, wurde 1958 von Rosenblatt
auf Grundlage der hebbschen Lernregel und dem McCulloch-Pitts-Neuron veröffentlicht.
Ein künstliches Neuron besteht aus folgenden Bestandteilen:

\begin{itemize}
\item Eingabe: Die Eingabe des Neurons wird mit einen Gewicht multipliziert.
                  Das Gewicht kann  positiv oder negativ sein. Wenn ein Gewicht
                  0 ist, ist die Verbindungen nicht existent.
\item Aktivierungsfunktion: Die Eingabe wird anschließend aufsummiert und in eine
                           Aktivierungsfunktion eingesetzt. Diese Aktivierungsfunktion
                           bestimmt die Ausgabe des Neurons.
\end{itemize}

Das künstliche Neuron wird mathematisch in (\ref{Formel:Aktivierungsfunktion})
beschrieben.
Die Aktivierungsfunktion ist $\varphi()$, $o_j$ die Ausgabe des Neurons,
$w_{ij}$ das Gewicht und $x_j$ die Eingabe des Neurons.

\begin{equation}
\begin{gathered}
o_j =  	\varphi(net_j) \\
net_j = \sum^n_{i=0} w_{ij} x_i
\end{gathered}
\label{Formel:Aktivierungsfunktion}
\end{equation}

Dieses Neuron kann einfache arithmetische und logische Rechenoperationen
ausführen, allerdings werden für Mustererkennungen und Klassifizierung mehrere
Neuronen benötigt.
Diese werden in einem Netzwerk zusammengeschaltet.
Die Neuronen werden in verschiedenen Schichten, sogenannte Layer, eingeteilt.
Die erste Schicht wird als Eingabeschicht bzw. Input Layer bezeichnet und die letzte
Schicht ist die Ausgabeschicht bzw. Output Layer.
Die Verbindungen der Neuronen können entweder auf Neuronen des nächsten
Layerns zeigen, sogenannte Feed-Forward-Netze und/oder auf Neuronen der gleichen
oder vorherigen Schicht zeigen, sogenannte rekursive Netze.

So ein Netz kann nicht mehr durch die einfache hebbsche Regel lernen,
da die Neuronen voneinander abhängig sind.
Daher wurde das sogenannte Backpropagation entwickelt, welches ein Spezialfall
des Gradientenabstiegsverfahren ist.
Der Backpropagationalgorithmus ist in drei Schritte unterteilt.
Als erstes wird dem Netz ein Datensatz übergeben und die Ausgabe
berechnet.
Dies wird als Forwardpropagation bezeichnet.
Im zweiten Schritt wird die Ausgabe des ersten Schrittes mit dem tatsächlichen
Ergebnis verglichen und anschließend berechnet die Fehlfunktion den Fehler zwischen der
Vorhersage und dem tatsächlichen Ergebnis.
Im letzten Schritt wird der Fehler aus der Ausgabeschicht bis zur Eingabeschicht
zurückgerechnet.
Die Gewichte der Neuronenverbindungen werden in Abhängigkeit des Fehlers
in die gewünschte Richtung korrigiert.
Dieser Schritt wird als Backpropagation bezeichnet.

\begin{figure}[ht]
 \centering
  \includegraphics[width=0.75\textwidth]{Grundlagen/images/neuronalesNetz.PNG}
 \caption[neuronales Netz]{Mehrere Neuronen können zu einem Netzwerk zusamengefasst
                          werden. Die Neuronen werden in verschiedenen Schichten
                          eingeteilt. In diesem Beispiel gehören zwei Neuronen in
                          die Eingabeschicht $U_{in}$, zwei Neuronen in das Hidden Layer
                          $U_{hidden}$ und ein Neuron in die Ausgabeschicht $U_{out}$.
                          Die Neuronen sind zur nächsten Schicht verbunden.
                          }
 \label{fig:neuronNetz}
 \end{figure}


 \subsection{Aktivierungsfunktion}
 \label{subsec:Aktivierungsfunktion}
 Eine Aktivierungsfunktion bestimmt die Ausgabe eines Neurons.
 Die allgemeine Aktivierungsfunktion ist in \eqref{Formel:Aktivierungsfunktion} definiert.
 Dabei ist $w_{ij}$ das Gewicht, $x_i$ die Eingabe des Neurons und $o_j$ die 
 Ausgabe des Neurons.

Damit diese für den Optimierungsprozess benutzt werden kann, muss die Funktion
 sowohl monoton als auch differenzierbar sein..
 Folgende Funktionen werden oft als Aktivierungsfunkionen verwendet:
 \begin{itemize}
 \item sigmoide Funktion
 \item Rectifier (ReLu)
 \item softmax
 \item binäre Schnellwertfunktion
 \end{itemize}

 \begin{figure}[ht]
  \centering
   \includegraphics[width=0.9\textwidth]{Grundlagen/images/aktivierungsfunktion.png}
  \caption[Aktivierungsfunktionen]{Diese drei Aktivierungsfunktionen werden für
  neuronale Netze benutzt. Dabei wird ReLu am häufigsten verwendet.}
  \label{fig:aktivierungsfunktion}
  \end{figure}


 Die sigmoide Funktion, welche in Abbildung \ref{fig:aktivierungsfunktion} visualisiert wurde,
 ist definiert als $\varphi(t) = \frac{1}{1 + e^{-t}}$, wobei
 $t$ die Eingabe des Neurons und $e$ die eulersche Zahl ist.
 Die Funktion ist dabei differenzierbar und monoton.
 Bei dieser Funktion wird empfohlen, dass die Eingabe zwischen $-1$ und $1$ liegt, da
 der Anstieg in diesen Bereich am stärksten ist und dies das Lernen beschleunigen kann.

 Die ReLu-Funktion, welche in Abbildung \ref{fig:aktivierungsfunktion} visualisiert wurde,
  ist definiert als $\varphi(t) = max(0, t)$, wobei $t$ als die Eingabe des Neurons definiert ist.
 ReLu selbst ist bei den Wert $0$ nicht differenzierbar.
 Dieses Problem kann gelöst werden, indem entweder $1$ oder $0$  für den undefinierten Wert der Ableitung
 festgelegt wird.
 ReLu wird bei Bilderkennung \cite{glorot2011deep}
 und bei der Sounderkennung \cite{toth2013phone}, \cite{maas2013rectifier} verwendet.

 Die binäre Schwelle, welche in Abbildung \ref{fig:aktivierungsfunktion} visualisiert ist,
 ist einer der ältesten Aktivierungsfunktion für neuronale Netze und wurde beim
 McClulloch-Pitts-Neuron, verwendet
  \cite{cowan1990discussion}. Die Funktion ist in (\ref{Formel:BinarSchwelle})  definiert.
 Allerdings ist diese Funktion nicht differenzierbar, weshalb sie in neueren Netzen nicht mehr verwendet wird.
 \begin{equation}
 \begin{gathered}
 \varphi(t) = \begin{cases}
                     \text{1} &\quad\text{$x \geq 0$} \\
                     \text{0} &\quad\text{$x < 0$}
               \end{cases}
 \end{gathered}
 \label{Formel:BinarSchwelle}
 \end{equation}

 Die softmax-Funktion wird in der Regel nur für die letzte Schicht verwendet.
 Die Funktion, welche in (\ref{Formel:softmax}) zu sehen ist, überführt dabei einen $K$-dimensionalen
 Vektor $\varphi(t)$ in einen Vektor $l$ mit $k$ Dimension.
 Die Dimension $k$ ist dabei die Anzahl der Klassen.
 Jede Komponente des Vektors $l$ beinhaltet die Wahrscheinlichkeit für die entsprechende
 Klasse.
 Die Summe aller Komponenten ist 1.


 \begin{equation}
 \begin{gathered}
 \varphi(t)_j = \frac{e^{tj}}{\sum ^k_{k=1} e^{tk}} ~\text{für}~ j=1,..,k
 \end{gathered}
 \label{Formel:softmax}
 \end{equation}

 \subsection{Fehler}

Die Gewichte des Netzes werden beim Lernvorgang in Abhängigkeit des Fehlers verbessert. 
Dazu wird der Ausgabewert des aktuellen Datensatzes mit den tatsächlichen Wert 
in ein Verhältnis gesetzt.
Diese Berechnung wird als Fehlfunktion oder Kostenfunktion bezeichnet. 
Die Funktion spiegelt wider, wie gut das Netz gelernt hat.

\begin{gather}
\varphi(t)_j = \frac{1}{n} \sum ^n _{i=1)} (y- \widetilde{y})^2
\label{Formel:MSE}
\end{gather}

Als Fehlfunktion können viele unterschiedliche Funktionen benutzt werden.
Einer der häufigsten Fehlfunktionen ist der \textit{mean squared error
}(\textit{MSE}) oder 
auch \textit{mittlere quadratische Abweichung} \eqref{Formel:MSE} \cite{netze}.
$\widetilde{y}$ ist der geschätzte Wert des Modells und y der tatsächliche Wert.
Ein kleiner Wert des \textit{MSE} bedeutet, dass der Bias und die Varianz des Modells klein sind und somit das Modell eine bessere Qualität besitzt. 

Eine alternative Fehlfunktion ist die kategorische Kreuzentropie (englisch 
categorical cross entropy) \cite{categorical}. 
Für eine diskrete Wahrscheinlichkeitsverteilung gilt die Formel \eqref{Formel:Kreuzentropie}.
$X$ sind alle zu klassifizierenden Daten, $p(x)$ die geschätzte Klasse für das Datum $x$ und $q(x)$  die tatsächliche Klasse für das Datum $q(x)$.
Dadurch werden zwei Wahrscheinlichkeitsverteilungen von einem Datensatz in ein 
Verhältnis gesetzt.

\begin{gather}
H(p,q) = - \sum_{x \in X} p(x) * \log q(x)
\label{Formel:Kreuzentropie}
\end{gather}


Damit die Wahrscheinlichkeit für Overfitting sinkt, wird die Fehlfunktion 
um einen Strafterm erweitert. 
Dieser Strafterm wird Regulation genannt.
Häufig wird die  \textit{L1} \eqref{Formel:l1} und die \textit{L2} Regulation \eqref{Formel:l2} verwendet.
$\lambda$ ist der Regulationsparameter und es gilt: $\lambda > 0$.
Ein häufiger Wert ist $\lambda = 0\text{,}001$
Loss bezeichnet hier die Fehlfunktion.

\begin{gather}
S=loss  + \lambda * \sum |w|
\label{Formel:l1}
\end{gather}

\begin{gather}
S=loss  + \lambda * \sum ||w||^2
\label{Formel:l2}
\end{gather}

Durch \textit{L2} nähern sich die Gewichte des Trainings den Wert null an.
Bei \textit{L1} werden einige Gewichte null, weshalb das Modell 
verkleinert wird, da einige Verbindungen zwischen Neuronen wegfallen.
\textit{L1} und \textit{L2} können kombiniert werden.

 

\subsection{Convolutional Neural Network}
Das \ac{CNN} ist ein neuronales Netz, welches um zwei spezielle Layer 
erweitert wurde:
das convolutionale Layer und das Pooling-Layer.

Der convolutionale Layer extrahiert aus den Bildern bestimmte Merkmale, 
welche in anderen Schichten verwendet werden.
Dazu werden in dem Layer diskrete lineare Filter (vgl. Abschnitt \ref{subsec:Filter}) \cite{deeplearning} verwendet.
Die Gewichte eines convolutionalen Layers sind identisch, wodurch weniger 
Gewichte codiert werden müssen und somit der Speicheraufwand verringert 
wird.
Das Ergebnis der Faltung wird in eine Aktivierungsfunktion (vgl. Abschnitt
\ref{subsec:Aktivierungsfunktion} eingesetzt.
Es wird häufig ReLu als Aktivierungsfunktion verwendet.

Das Pooling-Layer \cite{pooling} erhält als Eingabe die Merkmale eines 
convolutionalen Layers.
Die Aufgabe eines Pooling-Layers besteht darin ähnliche Merkmale 
zusammen zu fassen, wodurch überflüssige Informationen entfernt werden.
Dadurch werden Merkmale positionsunabhängig und daher kann das Netz besser 
abstrahieren. 
  
Die häufigste Poolingsstrategie ist das Max-Pooling.
Dabei wird ein Abschnitt beliebiger Größe beispielsweise $2 \times 2$ Pixeln betrachtet.
Aus diesem Abschnitt wird der größte Pixelwert übernommen, während diese Poolingstrategie die anderen Werte verwirft.
Durch  Pooling werden Bilder kleiner, wodurch sowohl der Speicher- als 
auch der Rechenaufwand sinkt.

 \begin{figure}[ht]
  \centering
   \includegraphics[width=0.75\textwidth]{Grundlagen/images/CNN.png}
  \caption[Aufbau eines CNN]{ Dieses Bild zeigt ein klassischen Aufbau eines CNN. Zusätzlich 
  zeigt es noch den Datenverlauf eines Bildes an.  \cite{pooling}}
  \label{fig:AufbauCNN}
  \end{figure}

Die Resultate des Pooling-Layers wird einer sogenannten Fully-connected Layer 
übergeben.
Dieses Layer ist ein neuronales Netz (vgl. \ref{subsec:NN}), welches die
Klassifizierung des Bildes übernimmt. 
Die Layer können unterschiedlich kombiniert werden. 
Der allgemeine Aufbau des Netzes ist in \eqref{Formel:CNN} zu sehen.
Der Vorteil von mehreren convolutionalen Schichten ist,
dass die Bilder stärker gefiltert werden und somit die Merkmale feiner sind.
Eine häufige Variante ist in \ref{fig:AufbauCNN} zu sehen.

\begin{gather}
Bild \rightarrow [Conv*M \rightarrow Pool]* N \rightarrow Full*K \rightarrow Klassifigation
\label{Formel:CNN}
\end{gather}



%Abschnitt mal überlesen
\subsection{Overfitting}
Overfitting (deutsch Überanpassung) bedeutet, dass das Modell mehr Parameter
benutzt oder verwendet als es insgesamt benötigt.
Dabei wird zwischen zwei Arten von Overfitting unterschieden:

\begin{itemize}
 \item Das Modell ist flexibler als erforderlich. Dies kann die Performanz des Modells
       verschlechtern.
 \item Durch die Verwendung von Merkmalen, welche kaum einen Einfluss auf die Klassifizierung
       haben, steigt die Fehleranfälligkeit des Modells. Durch diese Merkmale
       kann das Modell falsche Rückschlüsse ziehen.
\end{itemize}


Overfitting kann sehr unterschiedliche Ursachen haben.
So können bei der Merkmalskonstruktion Eigenschaften gewählt werden, welche
für die Klassifizierung keinen oder einen sehr geringen Einfluss haben.
So hat beispielsweise die Farbe der Tinte in der Unterschriftenerkennung keinen
Einfluss, da diese Merkmale in keiner Beziehung stehen.
Weiterhin sollte das Modell nicht zu komplex für die Daten sein. Dies
bedeutet, dass das Modell mit wenigen Parameter modelliert wird.
Dadurch sollen Merkmale mit hohen Informationsgehalt verstärkt einfließen.

Um Overfitting zu minimieren, gibt es verschiedene Maßnahmen, welche ergriffen
werden können.
So sollten die zur Verfügung stehenden Daten untersucht werden, damit die
wichtigsten Merkmale ermittelt werden können (\cite{chicco2017ten}).
Zudem sollte entschieden werden, welche Art von Algorithmus verwendet wird.
Am Anfang sollte der einfachste Algorithmus verwendet werden, da
die Ergebnisse leichter nachvollziehbar sind.
Weiterhin können die Daten mit statistischen Verfahren wie PCI die Daten transformieren
und somit die Wahrscheinlichkeit für Overfitting verringern.
Für bestimmte Modelle wie neuronale Netze existieren speziell entwickelte
Methoden wie zum Beispiel Dropout.




\subsection{Dropout}

Viele neuronale Netze nutzen eine hohe Zahl von trainierbaren Neuronen, wodurch
die Wahrscheinlichkeit für Overfitting steigt.
Es gibt bereits verschiedene Techniken zur Vermeidung von Overfitting beispielsweise
L1-Regulation oder das Abbrechen des Trainings bei einer geringen Fehlerquote.

\begin{figure}[ht]
  \includegraphics{Grundlagen/images/Dropout.png}
 \caption[Dropout neuronales Netz]{Links zeigt ein einfaches neuronales Netz
          mit zwei Hidden Layern. Rechts zeigt das neuronale Netz nach dem Dropout
          verwendet wurde. Dadurch existierten weniger Verbindungen und weniger
          trainierbare Parameter. Quelle: \cite{srivastava2014dropout}}
 \label{fig:dropoutNet}
 \end{figure}

Dropout ist eine Technik gegen Overfitting\cite{srivastava2014dropout}.
Der Begriff \glqq Dropout\grqq{} referenziert   das Entfernen von Neuronen aus den
sichtbaren und versteckten Schichten.
Dabei werden alle Verbindungen der entfernten Einheit und somit auch die dazugehörige Gewichte
entfernt  und es entsteht ein dünnes neuronales Netz.
Dieser Vorgang ist in (\ref{fig:dropoutNet}) visualisiert.
Die Auswahl der Einheiten passiert zufällig mit der Wahrscheinlichkeit von $p$.
Die Variable $p$ ist dabei entweder ein fester Wert, welcher von den anderen
Neuronen abhängig ist, oder er wird bei der Erstellung des Netzes von Entwickler
festgelegt.
Für das Hidden Layer wird von \cite{srivastava2014dropout}
die Wahrscheinlichkeit 0.5 empfohlen, da es für die meisten Aufgaben ausreicht.
Für die Eingabeschicht ist ein Wert näher bei 1.0 optimaler.
Durch Dropout entsteht ein neues neuronales Netzwerk, wobei ein mögliches Netz von $2^n$ mögliche Netze entsteht.

Wenn Dropout hinzugefügt wird, kann das neuronale Netz ähnlich wie ein normale Netze
trainiert werden.
Nach jeden Trainingsschritt wird durch das Entfernen der Neuronen ein neues dünneren
neuronales Netz erzeugt.
In Trainingsschritt wird Forward und Backpropagation ohne Änderung der Verfahren auf das ausgedünnte
neuronales Netz ausgeführt.
