import cv2 as cv
from matplotlib import pyplot as plt
import numpy as np

mendel = cv.imread("signature_0_25.jpg",0)
hauptmann = cv.imread("signature_0_43.jpg",0)
mangel = cv.imread("signature_0_54.jpg",0)
vitae = cv.imread("signature_0_89.jpg",0)


plt.subplot(2,2,1)
plt.imshow(mendel, cmap = 'gray', interpolation = 'bicubic')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
plt.subplot(2,2,2)
plt.imshow(hauptmann, cmap = 'gray', interpolation = 'bicubic')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
plt.subplot(2,2,3)
plt.imshow(mangel, cmap = 'gray', interpolation = 'bicubic')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
plt.subplot(2,2,4)
plt.imshow(vitae, cmap = 'gray', interpolation = 'bicubic')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
#plt.show()

plt.savefig('generierte_daten.png')
