from python:3.5

RUN mkdir /code
WORKDIR /code
ADD /workbench/signature_decetion /code/signature_decetion
WORKDIR /code/signature_decetion/
RUN pip install -r requirements.txt
WORKDIR /code/signature_decetion/src
