# Lehrer-Schüler-Beziehung (Arbeitstitel)

## Projektskizze

Die Frage von Lehrer-Schüler-Verbindungen ist für die musikhistorische
Forschung von hoher Bedeutung. Zwar geben die Zeugnisse Aufschluss über die
Frage, welche/r Schülerin/Schüler von welchem Lehrer unterrichtet
wurde. Allerdings wurden diese bislang nie systematisch und umfassend,
höchstens im Einzelfall ausgewertet. Unbeantwortet bleibt im Umkehrschluss
bislang die Frage, welche Schüler*innen insgesamt von einem einzelnen Lehrer
unterrichtet wurden.

Von einer Texterkennung und Auslese der Unterschriften auf den Zeugnissen
erhoffen wir uns hinsichtlich dieser Forschungsfrage Unterstützung. Durch
Clustern von ähnlichen Unterschriften und eine intellektuelle Zuordnung zu
Lehrernamen lassen sich bedeutende Informationen auslesen. Dabei stellt die
mögliche Anreicherung der Lehrernamen mit GND/VIAF-IDs einen zusätzlichen
Mehrwert dar.

## Datenquellen

* https://speicherwolke.uni-leipzig.de/index.php/s/2wjvzYDJyZWdzVm -
  Zeugnis-Scans
* https://data.dnb.de/opendata/ - GND als RDF (1.9 GB)

## Kontakte

* Anke Hofmann (Bibliotheksleiterin), <anke.hofmann@hmt-leipzig.de>
* Ingrid Jach (Archivmitarbeiterin), <ingrid.jach@hmt-leipzig.de>

## Forschungsfragen

* Zuordnung Schüler - Fach - Lehrer aus den Unterschriften auf den Zeugnissen
  herausbekommen. Wie das als RDF darstellen? Als Observations eines RDF Cube?
* Zuordnung der Schülerdaten zur GND (Ermittlung der jeweiligen ID in der PND)

## Termine

2018-12-11 Treffen mit Betreuer

* Arbeit von Nat. Philipp durchgesehen und auch das Problem mit ihm besprochen.
* Prof. Burghardt ist auf dem Gebiet kein Experte. Middendorf und Bogdan fragen.
* Middendorf verweist auf Tobias Jagla.
* Für die Rechnungen mit den Netzen wird mehr Rechenpower benötigt. 

2018-12-04 Treffen mit Betreuer

* Metadaten zu den Fächern wurden erfasst sowie weitere Informationen zu den
  Professoren aus Drittquellen (Wikipedia, GND)
* Beobachtungen bei der Analyse der Daten werden in eigener md-Datei
  zusammengetragen.
* Hinweis auf Masterarbeiten von Nathanael Phillip und Phillip Lang (beide
  ASV) zu CNN-Ansätzen zur Lokalisierung und Erkennung ägyptischer
  Hieroglyphen auf Bildern.
* Termin Prof. Burghardt noch offen. 

2018-11-27 Treffen mit Betreuer

* Übersicht der Schüler als RDF extrahiert und weiter angereichert,
  insbesondere auch Schüler mit mehreren Zeugnissen
* Kann das sinnvoll mit der GND
  <http://www.dnb.de/DE/Service/DigitaleDienste/LinkedData/linkeddata_node.html>
  abgeglichen werden?
* Liste der Lehrer aus dem bereitgestellten PDF extrahiert.
* Liste der Fächer ist über größere Zeiträume weitgehend konstant, kann
  mglw. ohne Technik genauer dargestellt werden.
* Leute fragen, die solche Mustererkennungen schon gemacht haben
  (Prof. Burghardt, Dr. Köntges, Dr. Jänicke).

2018-11-20 - Eröffnungsvortrag im BAMA. Aus der Diskussion

* Datenmengen sind zu klein für die propagierten KI-Verfahren

2018-12-17 Treffen mit Dr. Jänicke um 10 Uhr 
