#!/bin/bash
if [ $(dpkg-query -W -f='${Status}' unzip  2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  echo "Unzip not found.\nPlease install unzip with apt-get install unzip"
  exit
fi

if [ $(dpkg-query -W -f='${Status}' wget  2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  echo "Wget not found.\nPlease install wget with apt-get install wget"
  exit
fi

echo "Download metadata"
#wget -c "https://speicherwolke.uni-leipzig.de/index.php/s/2wjvzYDJyZWdzVm/download?files=HMT1-6200csv.csv" -O HMT1-6200csv.csv
echo "Download enrolments"
#wget -c "https://speicherwolke.uni-leipzig.de/index.php/s/2wjvzYDJyZWdzVm/download?files=Inskriptionen.zip" -O Inskriptionen.zip
echo "Download ressources"
wget -c "https://speicherwolke.uni-leipzig.de/index.php/s/2wjvzYDJyZWdzVm/download?files=Zeugnisse00000000.zip" -O Zeugnisse00000000.zip
wget -c "https://speicherwolke.uni-leipzig.de/index.php/s/2wjvzYDJyZWdzVm/download?files=Zeugnisse00001000.zip" -O Zeugnisse00000100.zip
wget -c "https://speicherwolke.uni-leipzig.de/index.php/s/2wjvzYDJyZWdzVm/download?files=Zeugnisse00002000.zip" -O Zeugnisse00000200.zip
wget -c "https://speicherwolke.uni-leipzig.de/index.php/s/2wjvzYDJyZWdzVm/download?files=Zeugnisse00004000.zip" -O Zeugnisse00000400.zip
wget -c "https://speicherwolke.uni-leipzig.de/index.php/s/2wjvzYDJyZWdzVm/download?files=Zeugnisse00005000.zip" -O Zeugnisse00000500.zip
wget -c "https://speicherwolke.uni-leipzig.de/index.php/s/2wjvzYDJyZWdzVm/download?files=Zeugnisse00003000.zip" -O Zeugnisse00000300.zip

echo "Move to Daten/Ressources"
mkdir Daten/Ressources/
mkdir Daten/Ressources/Zeugnisse
mkdir Daten/Ressources/Inskriptionen
mv Inskriptionen.zip Daten/Ressources/Inskriptionen
mv *.zip Daten/Ressources/Zeugnisse
mv HMT1-6200csv.csv Daten/Ressources

unzip Daten/Ressources/Inskriptionen/*.zip -d Daten/Ressources/Inskriptionen
rm Daten/Ressources/Inskriptionen/*.zip
unzip Daten/Ressources/Zeugnisse/Zeugnisse00000000.zip -d Daten/Ressources/Zeugnisse
unzip Daten/Ressources/Zeugnisse/Zeugnisse00000100.zip -d Daten/Ressources/Zeugnisse
unzip Daten/Ressources/Zeugnisse/Zeugnisse00000200.zip -d Daten/Ressources/Zeugnisse
unzip Daten/Ressources/Zeugnisse/Zeugnisse00000300.zip -d Daten/Ressources/Zeugnisse
unzip Daten/Ressources/Zeugnisse/Zeugnisse00000400.zip -d Daten/Ressources/Zeugnisse
unzip Daten/Ressources/Zeugnisse/Zeugnisse00000500.zip -d Daten/Ressources/Zeugnisse
#rm Daten/Ressources/Zeugnisse/*.zip
